<?php
/**
 * @file
 * Configuration for the Akamai Cloudlets API module.
 */

/**
 * Menu callback for the Akamai Cloudlets API settings form.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function akamai_cloudlets_settings() {
  $form = array();

  $form['akamai_cloudlets_help_text'] = array(
    '#markup' => t('<p>To generate these credentials, please follow the instructions on the <a href="@akamai-url" target="_blank">Authorize Your Client</a> page of the Akamai {OPEN} API documentation.</p>', array(
      '@akamai-url' => 'https://developer.akamai.com/introduction/Prov_Creds.html',
    )),
  );
  $form['akamai_cloudlets_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#description' => t('akaa-xxxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxx.luna.akamaiapis.net'),
    '#default_value' => variable_get('akamai_cloudlets_host', ''),
    '#required' => TRUE,
  );
  $form['akamai_cloudlets_client_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Client token'),
    '#description' => t('akab-xxxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxx'),
    '#default_value' => variable_get('akamai_cloudlets_client_token', ''),
    '#required' => TRUE,
  );
  $form['akamai_cloudlets_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#description' => t('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),
    '#default_value' => variable_get('akamai_cloudlets_client_secret', ''),
    '#required' => TRUE,
  );
  $form['akamai_cloudlets_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#description' => t('akab-xxxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxx'),
    '#default_value' => variable_get('akamai_cloudlets_access_token', ''),
    '#required' => TRUE,
  );
  $form['akamai_cloudlets_network'] = array(
    '#type' => 'radios',
    '#title' => t('Network'),
    '#description' => t('Whether requests should be made to the staging network or the production network. Please note that the staging network also points at your production domain. Please see <a href="@url" target="_blank">Testing on Akamai (Spoofing)</a> for more details.', array(
      '@url' => 'https://community.akamai.com/community/web-performance/blog/2015/06/15/testing-on-akamai-spoofing',
    )),
    '#options' => array(
      'staging' => t('Staging'),
      'production' => t('Production'),
    ),
    '#default_value' => variable_get('akamai_cloudlets_network', 'staging'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Menu callback for the Akamai Cloudlets API test page.
 *
 * @see akamai_cloudlets_connection_test_submit()
 *
 * @ingroup forms
 */
function akamai_cloudlets_connection_test() {
  $form = array();
  $form['#submit'][] = 'akamai_cloudlets_connection_test_submit';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test connection'),
  );

  return $form;
}

/**
 * Form submission handler for akamai_cloudlets_connection_test().
 *
 * @see akamai_cloudlets_connection_test()
 */
function akamai_cloudlets_connection_test_submit() {
  // Test a basic API endpoint to see if a connection can be established.
  $response = akamai_cloudlets_http_request('/cloudlets/api/v2/cloudlet-info');
  // If a 200 is returned, all is well.
  if ($response->code == '200') {
    drupal_set_message(t('Successfully connected.'));
  }
  // Otherwise, show the user an error and the raw response data.
  else {
    $response_output = print_r($response, TRUE);
    drupal_set_message(t('Unable to connect. Please verify your settings and try again. Raw response data: <pre>@response</pre>', array(
      '@response' => $response_output,
    )), 'error');
    watchdog('akamai_cloudlets', 'Unsuccessful connection test: <pre>@response</pre>', array(
      '@response' => $response_output,
    ), WATCHDOG_DEBUG);
  }
}
