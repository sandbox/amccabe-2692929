<?php
/**
 * @file
 * Contains AkamaiCloudletsPolicyVersion.
 */

/**
 * Class AkamaiCloudletsPolicyVersion.
 */
class AkamaiCloudletsPolicyVersion {

  /**
   * The description of this policy version.
   *
   * @var string
   */
  public $description;

  /**
   * The match rules attached to the policy version.
   *
   * @var array
   */
  public $matchRules;

  /**
   * The number of the policy version.
   *
   * @var int
   */
  protected $version;

  /**
   * The ID of the policy that the version is attached to.
   *
   * @var int
   */
  protected $policyId;

  /**
   * Whether or not the version is locked against further editing.
   *
   * @var bool
   */
  protected $rulesLocked;

  /**
   * AkamaiCloudletsPolicyVersion constructor.
   *
   * @param object $original
   *   (optional) The original policy version object from the Akamai API.
   */
  public function __construct(stdClass $original = NULL) {
    if (isset($original->description)) {
      $this->description = $original->description;
    }
    if (isset($original->matchRules)) {
      $this->matchRules = $original->matchRules;
      foreach ($this->matchRules as &$match_rule) {
        $match_rule = new AkamaiCloudletsERMatchRule($match_rule);
      }
    }
    if (isset($original->version)) {
      $this->version = $original->version;
    }
    if (isset($original->policyId)) {
      $this->policyId = $original->policyId;
    }
    if (isset($original->rulesLocked)) {
      $this->rulesLocked = $original->rulesLocked;
    }
  }

  /**
   * Get the version ID.
   *
   * @return int
   *   The version ID.
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Get the locked state of the version.
   *
   * @return bool
   *   TRUE if locked, FALSE otherwise.
   */
  public function getRulesLocked() {
    return $this->rulesLocked;
  }

  /**
   * Updates the policy version on Akamai.
   *
   * @return bool
   *   TRUE if the update was successful, FALSE otherwise.
   */
  public function update() {
    $url = '/cloudlets/api/v2/policies/' . $this->policyId . '/versions/' . $this->version;
    $query = array(
      'omitRules' => 'false',
      'matchRuleFormat' => '1.0',
    );
    $options = array(
      'method' => 'PUT',
      'data' => $this,
    );

    $response = akamai_cloudlets_http_request($url, $options, $query);
    if ($response->code == '200') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Clones the policy version.
   *
   * @param bool $include_rules
   *   (optional) Whether or not to include match rules. Defaults to TRUE.
   * @param string $match_format
   *   (optional) The format for the match rules. Defaults to 1.0.
   *
   * @return \AkamaiCloudletsPolicyVersion|false
   *   Either the cloned policy version, or FALSE if the cloning failed.
   */
  public function cloneVersion($include_rules = TRUE, $match_format = '1.0') {
    $url = '/cloudlets/api/v2/policies/' . $this->policyId . '/versions';
    // Copy the current object so the description can be modified.
    $policy_copy = clone $this;
    $policy_copy->description = 'Based on v' . $this->version . '.';
    $options = array(
      'method' => 'POST',
      'data' => $policy_copy,
    );
    $query = array(
      'includeRules' => !empty($include_rules) ? 'true' : 'false',
      'matchRuleFormat' => $match_format,
      'cloneVersion' => $this->version,
    );

    $response = akamai_cloudlets_http_request($url, $options, $query);
    if ($response->code == '201' && !empty($response->data)) {
      return new AkamaiCloudletsPolicyVersion($response->data);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Activate the policy version.
   *
   * @param string $network
   *   (optional) Which network to activate the version on. Defaults to
   *   'staging', can also be 'production'.
   *
   * @return bool
   *   TRUE if the activation was successful, FALSE otherwise.
   */
  public function activate($network = 'staging') {
    $url = '/cloudlets/api/v2/policies/' . $this->policyId . '/versions/' . $this->version . '/activations';
    $activation_data = new stdClass();
    $activation_data->network = $network;
    $options = array(
      'method' => 'POST',
      'data' => $activation_data,
    );

    $response = akamai_cloudlets_http_request($url, $options);
    if ($response->code == '200') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
