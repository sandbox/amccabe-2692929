<?php
/**
 * @file
 * Contains AkamaiCloudletsERMatchRule.
 */

/**
 * Class AkamaiCloudletsERMatchRule.
 */
class AkamaiCloudletsERMatchRule extends AkamaiCloudletsMatchRule {

  /**
   * The type of match rule.
   *
   * @var string
   */
  public $type;

  /**
   * The ID of the rule.
   *
   * @var int
   */
  public $id;

  /**
   * The name of the rule.
   *
   * @var string
   */
  public $name;

  /**
   * The URL to redirect from.
   *
   * @var string
   */
  public $matchURL;

  /**
   * The URL to redirect to.
   *
   * @var string
   */
  public $redirectURL;

  /**
   * When the rule should take effect.
   *
   * @var int
   */
  public $start;

  /**
   * When the rule should no longer take effect.
   *
   * @var int
   */
  public $end;

  /**
   * The HTTP status code to redirect with (301 or 302).
   *
   * @var int
   */
  public $statusCode;

  /**
   * Whether or not to preserve the incoming query string.
   *
   * @var bool
   */
  public $useIncomingQueryString;

  /**
   * AkamaiCloudletsERMatchRule constructor.
   *
   * @param object $original
   *   (optional) The original match rule object from the Akamai API.
   */
  public function __construct(stdClass $original = NULL) {
    $this->type = 'erMatchRule';

    if (isset($original->id)) {
      $this->id = $original->id;
    }

    if (isset($original->name)) {
      $this->name = $original->name;
    }

    if (isset($original->matchURL)) {
      $this->matchURL = $original->matchURL;
    }

    if (isset($original->redirectURL)) {
      $this->redirectURL = $original->redirectURL;
    }

    if (isset($original->start)) {
      $this->start = $original->start;
    }
    else {
      $this->start = 0;
    }

    if (isset($original->end)) {
      $this->end = $original->end;
    }
    else {
      $this->end = 0;
    }

    if (isset($original->statusCode)) {
      $this->statusCode = $original->statusCode;
    }
    else {
      $this->statusCode = 301;
    }

    if (isset($original->useIncomingQueryString)) {
      $this->useIncomingQueryString = $original->useIncomingQueryString;
    }
    else {
      $this->useIncomingQueryString = TRUE;
    }
  }

}
