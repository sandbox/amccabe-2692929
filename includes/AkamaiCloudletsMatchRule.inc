<?php
/**
 * @file
 * Contains AkamaiCloudletsMatchRule.
 *
 * @todo Move common properties from AkamaiCloudletsERMatchRule to here.
 */

/**
 * Class AkamaiCloudletsMatchRule.
 */
class AkamaiCloudletsMatchRule {

  /**
   * AkamaiCloudletsMatchRule constructor.
   *
   * @param object $original
   *   (optional) The original match rule object from the Akamai API.
   */
  public function __construct(stdClass $original = NULL) {
  }

}
