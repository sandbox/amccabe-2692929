<?php
/**
 * @file
 * Contains AkamaiCloudletsPolicy.
 */

/**
 * Class AkamaiCloudletsPolicy.
 */
class AkamaiCloudletsPolicy {

  /**
   * The name of this policy.
   *
   * @var string
   */
  public $name;

  /**
   * The description of this policy.
   *
   * @var string
   */
  public $description;

  /**
   * The ID of the policy.
   *
   * @var int
   */
  protected $policyId;

  /**
   * The code for the type of cloudlet that the policy is.
   *
   * @var string
   */
  protected $cloudletCode;

  /**
   * AkamaiCloudletsPolicy constructor.
   *
   * @param object $original
   *   (optional) The original policy object from the Akamai API.
   */
  public function __construct(stdClass $original = NULL) {
    if (isset($original->name)) {
      $this->name = $original->name;
    }
    if (isset($original->description)) {
      $this->description = $original->description;
    }
    if (isset($original->policyId)) {
      $this->policyId = $original->policyId;
    }
    if (isset($original->cloudletCode)) {
      $this->cloudletCode = $original->cloudletCode;
    }
  }

  /**
   * Get the ID of the policy.
   *
   * @return int
   *   The ID of the policy.
   */
  public function getPolicyId() {
    return $this->policyId;
  }

  /**
   * Get the cloudlet code of the policy.
   *
   * @return string
   *   The cloudlet code of the policy.
   */
  public function getCloudletCode() {
    return $this->cloudletCode;
  }

  /**
   * Update the policy on Akamai.
   *
   * @return bool
   *   TRUE if the update was successful, FALSE otherwise.
   */
  public function update() {
    $url = '/cloudlets/api/v2/policies/' . $this->policyId;
    $options = array(
      'method' => 'PUT',
      'data' => $this,
    );
    $response = akamai_cloudlets_http_request($url, $options);
    if ($response->code == '200') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Load all versions of the policy.
   *
   * @param bool $include_rules
   *   (optional) Whether or not to include match rules in the results. Defaults
   *   to TRUE.
   *
   * @return \AkamaiCloudletsPolicyVersion[]
   *   An array of version objects, or an empty array if no versions were found.
   */
  public function getVersions($include_rules = TRUE) {
    $url = '/cloudlets/api/v2/policies/' . $this->policyId . '/versions';
    $query = array(
      'includeRules' => !empty($include_rules) ? 'true' : 'false',
    );
    $response = akamai_cloudlets_http_request($url, array(), $query);
    $version_list = array();
    if ($response->code == '200' && !empty($response->data)) {
      foreach ($response->data as $version) {
        $version_list[] = new AkamaiCloudletsPolicyVersion($version);
      }
    }

    return $version_list;
  }

  /**
   * Get the latest version for this policy, if it is editable.
   *
   * @param bool $include_rules
   *   (optional) Whether or not to include match rules in the results. Defaults
   *   to TRUE.
   * @param bool $not_found_clone_latest
   *   (optional) Whether or not to clone the latest non-editable version if no
   *   editable version could be found. Defaults to FALSE.
   *
   * @return \AkamaiCloudletsPolicyVersion|false
   *   Returns an editable policy version object if found, FALSE otherwise.
   */
  public function getLatestEditableVersion($include_rules = TRUE, $not_found_clone_latest = FALSE) {
    // Get all versions attached to the policy.
    $version_list = $this->getVersions($include_rules);
    // Get the latest version from that list.
    /** @var AkamaiCloudletsPolicyVersion $latest_version */
    $latest_version = end($version_list);
    // Check if the latest version is editable.
    if (!empty($latest_version) && !$latest_version->getRulesLocked()) {
      return $latest_version;
    }
    // If no editable version was found and returned, check if a clone should be
    // made instead.
    if (!empty($latest_version) && !empty($not_found_clone_latest)) {
      return $latest_version->cloneVersion();
    }

    return FALSE;
  }

}
