<?php
/**
 * @file
 * Integrates the Akamai Cloudlets API into Drupal.
 */

define('AKAMAI_CLOUDLETS_MAX_BODY', '131072');

/**
 * Implements hook_menu().
 */
function akamai_cloudlets_menu() {
  $items = array();

  $items['admin/config/system/akamai-cloudlets'] = array(
    'title' => 'Akamai Cloudlets',
    'description' => 'Configure integration with the Akamai Cloudlets API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('akamai_cloudlets_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'akamai_cloudlets.admin.inc',
  );
  $items['admin/config/system/akamai-cloudlets/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/system/akamai-cloudlets/test'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Connection test',
    'description' => 'Test connection to the the Akamai Cloudlets API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('akamai_cloudlets_connection_test'),
    'access arguments' => array('administer site configuration'),
    'file' => 'akamai_cloudlets.admin.inc',
    'weight' => 10,
  );

  return $items;
}

/**
 * Sends a request to the Akamai API using drupal_http_request().
 *
 * @param string $url
 *   A string containing a relative URL, for example,
 *   '/cloudlets/api/v2/cloudlet-info'. If it does not begin with a forward
 *   slash ('/'), a forward slash will be prepended to it.
 * @param array $options
 *   (optional) An array that can have one or more of the following elements:
 *   - protocol: A string containing the request protocol. Defaults to 'https'.
 *   - Additional $options elements used by the drupal_http_request() function.
 * @param array $query
 *   (optional) An array of query parameters to be passed to
 *   drupal_http_build_query().
 *
 * @return object
 *   An object, as returned by drupal_http_request().
 *
 * @see drupal_http_request()
 * @see drupal_http_build_query()
 * @see https://developer.akamai.com/api/luna/cloudlets/reference.html
 */
function akamai_cloudlets_http_request($url, $options = array(), $query = array()) {
  // Load the settings needed.
  $host = variable_get('akamai_cloudlets_host', '');
  $client_token = variable_get('akamai_cloudlets_client_token', '');
  $client_secret = variable_get('akamai_cloudlets_client_secret', '');
  $access_token = variable_get('akamai_cloudlets_access_token', '');

  // Check for settings before proceeding.
  if (empty($host) || empty($client_token) || empty($client_secret) || empty($access_token)) {
    $response = new stdClass();
    $response->error = t('Akamai Cloudlets settings must be configured.');
    $response->code = -1001;

    return $response;
  }

  // Remove extraneous whitespace from the URL.
  $url = trim($url);
  // Ensure that the URL starts with a /.
  if (strpos($url, '/') !== 0) {
    $url = '/' . $url;
  }

  // Build the URL query parameters.
  if (!empty($query)) {
    $url .= '?' . drupal_http_build_query($query);
  }

  // Merge the default options. Borrowed from drupal_http_request().
  $options += array(
    'headers' => array(),
    'method' => 'GET',
    'data' => NULL,
    'max_redirects' => 3,
    'timeout' => 30.0,
    'context' => NULL,
    'protocol' => 'https',
  );

  // If data is present, it should be JSON-encoded.
  if (!empty($options['data'])) {
    $options['data'] = json_encode($options['data']);
  }

  // Get the protocol.
  $protocol = $options['protocol'];
  // Get the method and ensure it is uppercase.
  $method = drupal_strtoupper($options['method']);

  // Generate the timestamp in the correct format.
  $timestamp = date('Ymd\TH:i:sO');
  // Generate the signing key.
  $signing_key = akamai_cloudlets_base64_sha256($timestamp, $client_secret);

  // Build the Authorization header.
  $authorization_header = 'EG1-HMAC-SHA256 ';
  $authorization_header .= 'client_token=' . $client_token . ';';
  $authorization_header .= 'access_token=' . $access_token . ';';
  $authorization_header .= 'timestamp=' . $timestamp . ';';
  // @todo Implement optional UUID integration.
  $authorization_header .= 'nonce=' . uniqid() . ';';

  // Build the canonical headers for use in the signature.
  $canonical_headers = array();
  foreach ($options['headers'] as $header_name => $header_value) {
    $header_name = akamai_cloudlets_format_header($header_name, TRUE);
    $header_value = akamai_cloudlets_format_header($header_value);
    $canonical_headers[$header_name] = $header_name . ':' . $header_value;
  }

  // Only add the Content-Type header after building canonical headers.
  $options['headers']['Content-Type'] = 'application/json';

  // Use an empty string as the hash body for all methods except POST.
  $hashed_data = '';
  if ($method == 'POST') {
    // Truncate the request body if necessary.
    $options['data'] = substr($options['data'], 0, AKAMAI_CLOUDLETS_MAX_BODY);
    // Hash the body.
    $hashed_data = akamai_cloudlets_base64_sha256($options['data']);
  }

  // Build the signature.
  $signature_parts = array(
    $method,
    $protocol,
    $host,
    $url,
    implode("\t", $canonical_headers),
    $hashed_data,
    $authorization_header,
  );
  $signature = akamai_cloudlets_base64_sha256(implode("\t", $signature_parts), $signing_key);
  // Finish the authorization header.
  $authorization_header .= 'signature=' . $signature;
  $options['headers']['Authorization'] = $authorization_header;

  // Build the request URL and send the request to Akamai.
  $request_url = $protocol . '://' . $host . $url;
  $response = drupal_http_request($request_url, $options);
  // Decode the response data before returning it.
  if (!empty($response->data)) {
    $response->data = json_decode($response->data);
  }

  return $response;
}

/**
 * Formats header names and values.
 *
 * @param string $text
 *   The header name or value to clean.
 * @param bool $is_name
 *   TRUE if $text is a name, FALSE otherwise.
 *
 * @return string
 *   The formatted name or value.
 */
function akamai_cloudlets_format_header($text, $is_name = FALSE) {
  $text = trim($text);
  $text = preg_replace('/\s+/', ' ', $text);
  if (!empty($is_name)) {
    $text = drupal_strtolower($text);
  }

  return $text;
}

/**
 * Hashes data, and then base64 encodes it.
 *
 * @param string $data
 *   The data to be hashed.
 * @param string $key
 *   (optional) To generate a HMAC hash, provide a key. Otherwise, a normal hash
 *   will be generated.
 *
 * @return string
 *   A string containing the base64 encoded version of the hashed data.
 */
function akamai_cloudlets_base64_sha256($data, $key = '') {
  if (!empty($key)) {
    return base64_encode(hash_hmac('sha256', $data, $key, TRUE));
  }
  else {
    return base64_encode(hash('sha256', $data, TRUE));
  }
}

/**
 * Load all available policies from Akamai.
 *
 * @return \AkamaiCloudletsPolicy[]
 *   An array of policy objects, or an empty array if no policies were found.
 */
function akamai_cloudlets_load_policies() {
  $policy_list = array();
  $response = akamai_cloudlets_http_request('/cloudlets/api/v2/policies');
  if ($response->code == '200' && !empty($response->data)) {
    foreach ($response->data as $policy) {
      $policy_list[] = new AkamaiCloudletsPolicy($policy);
    }
  }

  return $policy_list;
}

/**
 * Load a single policy from Akamai.
 *
 * @param int $policy_id
 *   The ID of the policy to load.
 *
 * @return \AkamaiCloudletsPolicy|false
 *   A policy object if successful, or FALSE otherwise.
 */
function akamai_cloudlets_load_policy($policy_id) {
  // Sanitize the policy ID parameter.
  $policy_id = check_plain($policy_id);
  $policy_id = (int) $policy_id;
  if (empty($policy_id) || !is_int($policy_id)) {
    return FALSE;
  }
  // Attempt to load the policy from Akamai.
  $response = akamai_cloudlets_http_request('/cloudlets/api/v2/policies/' . $policy_id);
  // Return the policy if successful.
  if ($response->code == '200' && !empty($response->data)) {
    return new AkamaiCloudletsPolicy($response->data);
  }
  // Otherwise, return FALSE.
  else {
    return FALSE;
  }
}
